let regex = /^-?\d*(\.\d+)?$/;

let result = document.getElementsByName("total_price")[0];
let pricePerBox = document.getElementsByName("price_per_box")[0];
let kgPerBox = document.getElementsByName("kg_per_box")[0];
let fare1PerBox = document.getElementsByName("fare1_eur_per_box")[0];
let fare2PerKm = document.getElementsByName("fare2_eur_per_km")[0];
let distanceKm = document.getElementsByName("distance_km")[0];
let parcelNumber = document.getElementsByName("colis")[0];

let inputArray = [pricePerBox, kgPerBox, fare1PerBox, 
                fare2PerKm, distanceKm, parcelNumber];

let refresh_btn = document.getElementById("refreshButton");


// Event listeners
for (var i = 0 ; i < inputArray.length ; ++i) {
    (function(index) {
        inputArray[index].addEventListener("input", function() {
            calc(inputArray, result);
        });
    }) (i);
}

refresh_btn.addEventListener("click", function(){ 
    calc(inputArray, result);
});



// functions
function calc(inputArray, result) {
    let check = true;
    for (i in inputArray) {
        if( !validity_check(inputArray[i], result) ) {
            check = false;
            break;
        }
    }

    if(check) {
        result.value = (inputArray[0].value * inputArray[5].value) + 
                        (inputArray[2].value * inputArray[5].value) +
                        (inputArray[3].value * inputArray[4].value)
    }
}

function validity_check (item, result) {

    let check = true;

    console.log(item.value);
    if ( !item.value.match(regex) || item.value < 0 ) {
        item.classList.add("bg-danger", "text-white");
        result.value = "Certaines valeurs ne sont pas valides";
        check = false;
    }
    else {
        item.classList.remove("bg-danger", "text-white");
        check = true;
    }

    return check;
}